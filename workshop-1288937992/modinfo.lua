name = "Transparent pillars"
description = "Sets transparency pillars for a comfortable view. Any objects are clickable through pillars.\nYou can adjust transparency level in options."
author = "surg"
version = "1.6.3"
forumthread = ""
api_version_dst = 10
icon_atlas = "modicon.xml"
icon = "modicon.tex"
priority = 0
dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false
hamlet_compatible = false
dst_compatible = true
all_clients_require_mod = false
client_only_mod = true
server_filter_tags = {""}

local alpha_options = {}
for i = 1, 10 do
    alpha_options[i] = { description = (i * 10).."%", data = (i * 0.1) }
end

configuration_options =
{
    {
        name    = "ALPHA_CAVE_EXIT",
        label   = "Cave exit",
        hover   = "Sets alpha for cave exit",
        options = alpha_options,
        default = 0.5
    },
    {
        name    = "ALPHA_PILLAR_ALGAE",
        label   = "Algae pillar",
        hover   = "Sets alpha for algae pillar",
        options = alpha_options,
        default = 0.5
    },
    {
        name    = "ALPHA_PILLAR_ATRIUM",
        label   = "Atrium pillar",
        hover   = "Sets alpha for atrium pillar",
        options = alpha_options,
        default = 0.3
    },
    {
        name    = "ALPHA_PILLAR_CAVE",
        label   = "Cave pillar",
        hover   = "Sets alpha for cave pillar",
        options = alpha_options,
        default = 0.8
    },
    {
        name    = "ALPHA_PILLAR_CAVE_FLINTLESS",
        label   = "Cave flintless pillar",
        hover   = "Sets alpha for cave flintless pillar",
        options = alpha_options,
        default = 0.8
    },
    {
        name    = "ALPHA_PILLAR_CAVE_ROCK",
        label   = "Cave rock pillar",
        hover   = "Sets alpha for cave rock pillar",
        options = alpha_options,
        default = 0.8
    },
    {
        name    = "ALPHA_PILLAR_RUINS",
        label   = "Ruins pillar",
        hover   = "Sets alpha for ruins pillar",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_DAYWALKER_PILLAR",
        label   = "Cracked pillar",
        hover   = "Sets alpha for cracked pillar",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_PILLAR_SUPPORT",
        label   = "Support pillar",
        hover   = "Sets alpha for support pillar",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_PILLAR_SUPPORT_DREADSTONE",
        label   = "Support dreadstone pillar",
        hover   = "Sets alpha for support dreadstone pillar",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_PILLAR_STALACTITE",
        label   = "Stalactite pillar",
        hover   = "Sets alpha for stalactite pillar",
        options = alpha_options,
        default = 0.8
    },
    {
        name    = "ALPHA_TENTACLE_PILLAR",
        label   = "Tentacle pillar",
        hover   = "Sets alpha for tentacle pillar",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_ARCHIVE_PILLAR",
        label   = "Archive pillar",
        hover   = "Sets alpha for archive pillar",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_ARCHIVE_CHANDELIER",
        label   = "Archive chandelier",
        hover   = "Sets alpha for archive chandelier",
        options = alpha_options,
        default = 0.3
    },
    {
        name    = "ALPHA_VITREOASIS",
        label   = "Vitreoasis",
        hover   = "Sets alpha for vitreoasis",
        options = alpha_options,
        default = 0.5
    },
    {
        name    = "ALPHA_MAST",
        label   = "Mast",
        hover   = "Sets alpha for mast",
        options = alpha_options,
        default = 0.5
    },
    {
        name    = "ALPHA_MAST_MALBATROSS",
        label   = "Mast malbatross",
        hover   = "Sets alpha for mast malbatross",
        options = alpha_options,
        default = 0.5
    },
    {
        name    = "ALPHA_MASTUPGRADE",
        label   = "Mast upgrades",
        hover   = "Sets alpha for mast upgrades (lamp, lightningrod)",
        options = alpha_options,
        default = 0.5
    },
    {
        name    = "ALPHA_MONKEY_PILLAR",
        label   = "Monkey pillar",
        hover   = "Sets alpha for monkey pillar",
        options = alpha_options,
        default = 0.5
    },
    {
        name    = "ALPHA_OCEANVINE",
        label   = "Mossy vine",
        hover   = "Sets alpha for mossy vine",
        options = alpha_options,
        default = 0.3
    },
    {
        name    = "ALPHA_OCEANVINE_DECO",
        label   = "Oceanvine deco",
        hover   = "Sets alpha for oceanvine deco",
        options = alpha_options,
        default = 0.3
    },
    {
        name    = "ALPHA_OCEANVINE_COCOON",
        label   = "Oceanvine cocoon",
        hover   = "Sets alpha for oceanvine cocoon",
        options = alpha_options,
        default = 0.5
    },
    {
        name    = "ALPHA_OCEANTREE",
        label   = "Knobbly tree",
        hover   = "Sets alpha for knobbly tree",
        options = alpha_options,
        default = 0.5
    },
    {
        name    = "ALPHA_OCEANTREE_PILLAR",
        label   = "Above-average tree trunk",
        hover   = "Sets alpha for above-average tree trunk",
        options = alpha_options,
        default = 0.5
    },
    {
        name    = "ALPHA_EVERGREEN", 
        label   = "Evergreen",
        hover   = "Sets alpha for evergreen",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_EVERGREEN_SPARSE", 
        label   = "Lumpy evergreen",
        hover   = "Sets alpha for lumpy evergreen",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_DECIDUOUSTREE", 
        label   = "Birchnut tree",
        hover   = "Sets alpha for birchnut tree",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_ROCK_PETRIFIED_TREE", 
        label   = "Petrified tree",
        hover   = "Sets alpha for petrified tree",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_MOON_TREE", 
        label   = "Lune tree",
        hover   = "Sets alpha for lune tree",
        options = alpha_options,
        default = 0.6
    },
    {
        name    = "ALPHA_SANDSPIKE",
        label   = "Sand spike",
        hover   = "Sets alpha for sand spike",
        options = alpha_options,
        default = 0.4
    },
    {
        name    = "ALPHA_SANDBLOCK",
        label   = "Sand castle",
        hover   = "Sets alpha for sand castle",
        options = alpha_options,
        default = 0.1
    },
    {
        name    = "ALPHA_PIGHOUSE",
        label   = "Pig house",
        hover   = "Sets alpha for pig house",
        options = alpha_options,
        default = 0.8
    },
    {
        name    = "ALPHA_RABBITHOUSE",
        label   = "Rabbit hutch",
        hover   = "Sets alpha for rabbit hutch",
        options = alpha_options,
        default = 0.8
    },
    {
        name    = "ALPHA_MERMHOUSE",
        label   = "Leaky shack",
        hover   = "Sets alpha for leaky shack",
        options = alpha_options,
        default = 0.8
    },
    {
        name    = "ALPHA_MERMHOUSE_CRAFTED",
        label   = "Craftsmerm house",
        hover   = "Sets alpha for craftsmerm house",
        options = alpha_options,
        default = 0.8
    },
    {
        name    = "ALPHA_MERMWATCHTOWER",
        label   = "Merm Flort-ifications",
        hover   = "Sets alpha for merm flort-ifications",
        options = alpha_options,
        default = 0.8
    },    
    {
        name    = "ALPHA_CANOPY_TRANSPARENCY",
        label   = "Canopy (HUD)",
        hover   = "Sets alpha for canopy (HUD)",
        options = alpha_options,
        default = 0.1
    },
}
